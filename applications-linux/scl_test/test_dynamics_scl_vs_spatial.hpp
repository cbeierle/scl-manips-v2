/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
/*
 * test_dynamics_scl_vs_spatial.hpp
 *
 *  Created on: Jun 12, 2014
 *
 *  Copyright (C) 2014
 *
 *  Author : Nayan Singhal <singhalnayan91@gmail.com>
 *              Brains in Silicon Lab,
 *              Stanford University.
 *
 *  Edited by: Samir Menon <smenon@stanford.edu>
 */
#ifndef TESTDYNAMICSSCLVSSPATIAL_H_
#define TESTDYNAMICSSCLVSSPATIAL_H_

namespace scl_test
{
    void test_dynamics_scl_vs_spatial(int id);

} /* namespace scl_test */

#endif /* TESTDYNAMICSSCLVSSPATIAL_H_ */
